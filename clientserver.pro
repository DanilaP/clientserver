QT += core
QT -= gui

CONFIG += c++11

TARGET = clientserver
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    echoSK.cc \
    sendSK.cc \
    server.cpp \
    client.cpp

HEADERS += \
    send.hh

DISTFILES += \
    send.idl \
    README.md
