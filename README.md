#### Как запустить программу в операционной среде UNIX

1) Создать конфигурацию omniORB и установить переменную среды OMNIORB_CONFIG
Нужно создать файл config.cfg с единственной строкой:

    InitRef = NameService=corbaname::localhost

Переменная среды устанавливается следующей командой:

    export OMNIORB_CONFIG=/path/to/config.cfg

2) Запустить службу omniNames в отдельном окне консоли:

    mkdir ~/tmplog
    omniNames -start -logdir ~/tmplog

3) Скомпилировать проект с помощью QT Creator
Сначала нужно задать путь к библиотеке omniORB в настройках:
В меню Projects->Build Environment->Details:

    OMNIDIR=/path/to/omniORB

Потом собрать проект (Build->Build project)

4) Запустить сервер:

    ./server

5) В новом окне консоли запустить программу-клиент

    ./client

Вводимые символы мгновенно отображаются в окне server
