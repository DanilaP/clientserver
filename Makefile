OMNIDIR=/Users/dpotapov/install/omniorb
CC=g++
CFLAGS=-I$(OMNIINSTALL)/include -L$(OMNIINSTALL)/lib
all: client server
client: client.cpp sendSK.cc send.hh
	$(CC) $(CFLAGS) -lomniORB4 -lCOS4 -lomnithread -o client client.cpp sendSK.cc -lcurses
server: server.cpp sendSK.cc send.hh
	$(CC) $(CFLAGS) -lomniORB4 -lCOS4 -lomnithread -o server server.cpp sendSK.cc
clean:
	rm -f client server
